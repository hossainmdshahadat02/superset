FROM oraclelinux:8
# Copy your script into the container
COPY script.sh /usr/local/bin/script.sh
RUN yum update -y
RUN dnf install gcc gcc-c++ libffi-devel openssl-devel cyrus-sasl-devel openldap-devel python3.11 python3.11-pip python3.11-setuptools python3.11-wheel python3.11-devel
RUN python3.11 -m venv venv && \
    python3.11 -m pip install --upgrade setuptools pip && \
    python3.11 -m pip install --upgrade oracledb && \
    python3.11 -m pip install --upgrade apache-superset

# Make the script executable
RUN chmod +x /usr/local/bin/script.sh

# Run the script when the container starts
CMD ["script.sh"]
